import { Observable } from 'rxjs';

export const createHttpObservable = (url: string) => {
  return Observable.create( observer => {
    // AbortController is part of the fetch API and allow as to abort an HTTP call
    const controller = new AbortController();
    const signal = controller.signal;
    fetch(url, {signal})
      .then(response => {
        if (response.ok) {
          console.log(response.body);
          return response.json();
        } else {
          observer.error(`Request failed with status code ${response.status}`);
        }
      })
      .then(bodyJson => {
        observer.next(bodyJson);
        observer.complete();
      })
      .catch(err => {
        observer.error(err);
      });

      // The return option of an observable must be always a function
      // and is the function executed when you call the unsubscribe() method
      return () => controller.abort();

  });
};
