import { Injectable } from '@angular/core';
import { Course } from '../model/course';
import { Observable, BehaviorSubject } from 'rxjs';
import { createHttpObservable } from './util';
import { tap, map, filter } from 'rxjs/operators';
import { fromPromise } from 'rxjs/internal-compatibility';


@Injectable({
  providedIn: 'root'
})
export class Store {

  // BehaviorSubject keep track the last value emitted and that
  // is the one which is going to be returned when another subscriber
  // appears
  private subject = new BehaviorSubject<Course[]>([]);
  courses$: Observable<Course[]> = this.subject.asObservable() as Observable<Course[]>;

  init() {
    const http$ = createHttpObservable('/api/courses');
    http$
      .pipe(
        tap(() => console.log('HTTP request executed')),
        map(res => Object.values(res['payload'])),
        // Share parents subscriptions to avoid multiple calls
        //    shareReplay(),
        // retryWhen allow observables to retry the call after it fails
        // based on some logic described on the block
        // This will retry the observable inmediatly -> retryWhen(error => error)
        //    retryWhen(error => error.pipe(
        //      delayWhen(() => timer(200))
        //    ))
        // CATCH AND RETHROW ERROR HANDLING
        // catchError must return another observable to which continue
        // the subscription.
        // If moving catch block to the begining of the chain, it will omit
        // next calls if the first subscription triggers error.
        //    catchError(err => {
          // Observable return
          /* return of([
              {
                id: 2,
                description: 'Angular Security Course - Web Security Fundamentals',
                longDescription: 'Learn Web Security Fundamentals and apply them to defend an Angular / Node Application from multiple types of attacks.',
                iconUrl: 'https://s3-us-west-1.amazonaws.com/angular-university/course-images/security-cover-small-v2.png',
                courseListIcon: 'https://s3-us-west-1.amazonaws.com/angular-university/course-images/lock-v2.png',
                category: 'ADVANCED',
                lessonsCount: 11
              }
            ]); */

            //    console.log('Error occurs ', err);
            // Triggers angular error format on the console with custom message
            //     return throwError(err);
          // }),
          //    finalize(() => console.log('Request finalized.'))
      )
      .subscribe(courses => this.subject.next(courses));

  }

  selectBeginnerCourses() {
    return this.filterByCategory('BEGINNER');
  }

  selectAdvancedCourses() {
    return this.filterByCategory('ADVANCED');
  }

  selectCourseById(courseId: number): Observable<Course> {
    return this.courses$
            .pipe(
              map(courses => courses.find(course => course.id === courseId)),
              filter(course => !!course)
            );
  }

  filterByCategory(category: string) {
    return this.courses$
          .pipe(
            map(courses => courses.filter(course => course.category === category))
          );
  }

  saveCourse(idCourse: number, changes: any): Observable<any> {
    const courses = this.subject.getValue();
    const courseIndex = courses.findIndex(courseTmp => courseTmp.id === idCourse);
    const coursesModified = courses.slice(0);
    coursesModified[courseIndex] = {...courses[courseIndex], ...changes};
    this.subject.next(coursesModified);

    return fromPromise(fetch(`/api/courses/${idCourse}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(changes)
    }));
  }

}
