import { Store } from './../common/store.service';
import { debug, RxJsLoggingLevel, setRxJsLoggingLevel } from './../common/debug';
import { createHttpObservable } from './../common/util';
import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Course } from '../model/course';
import {
  debounceTime,
  distinctUntilChanged,
  startWith,
  tap,
  delay,
  map,
  concatMap,
  switchMap,
  withLatestFrom,
  concatAll,
  shareReplay,
  throttle,
  throttleTime,
  first,
  take
} from 'rxjs/operators';
import { merge, fromEvent, Observable, concat } from 'rxjs';
import { Lesson } from '../model/lesson';

@Component({
  selector: 'course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit, AfterViewInit {

  course$: Observable<Course>;
  lessons$: Observable<Lesson[]>;
  courseId: number;

  @ViewChild('searchInput')
  input: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private store: Store
  ) {}

  ngOnInit() {

    try {
      this.courseId = parseInt(this.route.snapshot.params['id'], null);
    } catch (e) {
      this.courseId = null;
    }
    setRxJsLoggingLevel(RxJsLoggingLevel.DEBUG);
    // Using store to retrieve values instead of api calls
    this.course$ = this.store.selectCourseById(this.courseId)
                                .pipe(
                                  // Force the completition of an Observable and
                                  // returns only the first value emitted
                                  //    first(),
                                  // So similar to first, the difference is that it returns
                                  // as many values as you define on the take()
                                  take(1)
                                );

    this.loadLessons()
            .pipe(
              // Combine two resulting observables adding but
              // with the last result emmited by the second observable
              withLatestFrom(this.course$)
            )
            .subscribe(([lessons, course]) => {
              console.log('lessons', lessons);
              console.log('course', course);
            });

    // Plain RxJs observable method
    /* this.course$ = createHttpObservable(`/api/courses/${this.courseId}`)
                      .pipe(
                        debug(RxJsLoggingLevel.INFO, 'course value ')
                      ); */
  }

  loadLessons(search = ''): Observable<Lesson[]> {
    return createHttpObservable(`/api/lessons?courseId=${this.courseId}&pageSize=100&filter=${search}`)
              .pipe(
                map(response => response['payload'])
              );
  }

  ngAfterViewInit() {
    this.lessons$ = fromEvent<any>(this.input.nativeElement, 'keyup')
                      .pipe(
                        map(event => event.target.value),
                        startWith(''),
                        debug(RxJsLoggingLevel.TRACE, 'search '),
                        // Delay the values emited from the observable and drops
                        // any other emition arrived before time elapsed but doesn't
                        // ensure you get the lastest output of the stream
                        //       throttleTime(500)
                        // Delay the values emited from the observable and drops
                        // any other emition arrived before time elapsed, and also
                        // ensure you get the lastest output of the stream
                        debounceTime(500),
                        // Check same value emited to avoid emit repeated values
                        distinctUntilChanged(),
                        // switchMap executes the observables sequentially but
                        // replacing/unsubscribing the actual to start the next one
                        // if another arrives before its completition
                        switchMap(search => this.loadLessons(search)),
                        debug(RxJsLoggingLevel.DEBUG, 'lessons value '),
                      );

    // const initialLesson$ = this.loadLessons();
    // this.lessons$ = concat(initialLesson$, searchLessons$);
  }
}
