import {AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Course} from '../model/course';
import {FormBuilder, Validators, FormGroup} from '@angular/forms';
import * as moment from 'moment';
import {fromEvent} from 'rxjs';
import {concatMap, distinctUntilChanged, exhaustMap, filter, mergeMap} from 'rxjs/operators';
import {fromPromise} from 'rxjs/internal-compatibility';
import { Store } from '../common/store.service';

@Component({
    selector: 'course-dialog',
    templateUrl: './course-dialog.component.html',
    styleUrls: ['./course-dialog.component.css']
})
export class CourseDialogComponent implements OnInit, AfterViewInit {

    form: FormGroup;
    course: Course;

    @ViewChild('saveButton') saveButton: ElementRef;

    @ViewChild('searchInput') searchInput: ElementRef;

    constructor(
        private fb: FormBuilder,
        private store: Store,
        private dialogRef: MatDialogRef<CourseDialogComponent>,
        @Inject(MAT_DIALOG_DATA) course: Course ) {

        this.course = course;

        this.form = fb.group({
            description: [course.description, Validators.required],
            category: [course.category, Validators.required],
            releasedAt: [moment(), Validators.required],
            longDescription: [course.longDescription, Validators.required]
        });

    }

    ngOnInit() {

      // This shows how to subscribe to event changes on the formulary
      // allowing as to trigger API request with the changes on each
      // field modification.
      /*this.form.valueChanges
                  .pipe(
                    filter(() => this.form.valid),
                    // concatMap executes sequentials observers
                    concatMap(changes => this.saveCourse(changes))
                    // mergeMap executes parallels observers
                    // mergeMap(changes => this.saveCourse(changes))
                  ).subscribe();
                  // BAD WAY NEST SUBSCRIBES()
                  // AN ANTIPATTERN FOR REACTIVE PROGRAMMING
                  /* .subscribe(changes => {
                      const saveCourse$ = fromPromise(fetch(`/api/courses/${this.course.id}`, {
                      method: 'PUT',
                      headers: {
                        'Content-Type': 'application/json'
                      },
                      body: JSON.stringify(changes)
                    }));
                    saveCourse$.subscribe();
                  });*/

    }

    saveCourse(changes) {
      return this.store.saveCourse(this.course.id, this.form.value);
    }

    ngAfterViewInit() {

      fromEvent(this.saveButton.nativeElement, 'click')
          .pipe(
            // exhaustMap ignore next observables as long as the actual is still running
            exhaustMap(() => this.saveCourse(this.form.value))
          )
          .subscribe(
            () => this.close(),
            err => console.log('Error saving course', err)
          );

    }



    close() {
        this.dialogRef.close();
    }

}
