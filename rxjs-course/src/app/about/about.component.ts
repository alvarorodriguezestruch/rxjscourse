import { createHttpObservable } from './../common/util';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject, AsyncSubject, ReplaySubject, interval, timer, fromEvent, Observable, noop, of, concat, merge } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    // When variable finish on $ means that its a Observable variable.
    const interval$ = interval(1000);
    const intrervalSubscription = interval$.subscribe(val => console.log('Stream interval: ' + val));
    setTimeout(() => {
      intrervalSubscription.unsubscribe();
    }, 5000);

    const timeout$ = timer(3000, 1000);
    const timeoutSubscription = timeout$.subscribe(val => console.log('Stream timeout: ' + val));
    timeoutSubscription.unsubscribe();

    const event$ = fromEvent(document, 'click');
    const eventSubscription = event$.subscribe(
      evt => console.log(evt), // Normal callback for subscribe
      err => console.log(err), // Error handling when error occurs
      () => console.log('Completed!') // Optional param triggered after completition of the subscribe if no errors.
    );
    eventSubscription.unsubscribe();

    // concat rxjs function
    // Executes observables sequentially
    const source1$ = of(1, 2, 3);
    const source2$ = of(4, 5, 6);
    const source3$ = of(7, 8, 9);
    // concat needs observables that completes to trigger and concat next observable
    // Ej: will never concat source2$ if first observable is an interval.
    const resultConcat$: Observable<number> = concat(source1$, source2$, source3$);
    const resultConcatSubscription = resultConcat$.subscribe(console.log);
    resultConcatSubscription.unsubscribe();


    // merge rxjs function
    // Executes observables in parallel
    const interval1$ = interval(1000);
    const interval2$ = interval1$.pipe(map(val => 10 * val));
    const resultMerge$ = merge(interval1$, interval2$);
    const resultMergeSubscription = resultMerge$.subscribe(console.log);
    resultMergeSubscription.unsubscribe();


    // Implementing cancellabel observables
    const interval10$ = interval(1000);
    const interval10Subscription = interval10$.subscribe(console.log);
    setTimeout(() => interval10Subscription.unsubscribe(), 5000);


    // Trying cancellable HTTP observables
    const http2$ = createHttpObservable('/api/courses');
    const http2Subscription = http2$.subscribe(console.log);
    setTimeout(() => http2Subscription.unsubscribe(), 0);


    // Subjects are just like Observable, the difference is they
    // act as Observable and as Observer which means that the can emit values,
    // and complete() the observable but can also subscribe to itself
    // IMPORTANT: Try to use as less as possible
    const subject = new Subject();
    const series$ = subject.asObservable();
    series$.subscribe(console.log);

    subject.next(1);
    subject.next(2);
    subject.next(3);
    subject.complete();



    // AsyncSubjects just returns the last value emitted of the stream
    // Ideal for long term calculations
    const asyncSubject = new AsyncSubject();
    const asyncSeries$ = subject.asObservable();
    asyncSeries$.subscribe(val => console.log(`First subscriber: ${val}`));

    asyncSubject.next(1);
    asyncSubject.next(2);
    asyncSubject.next(3);
    asyncSubject.complete();

    setTimeout(() => {
        asyncSeries$.subscribe(val => console.log(`Second subscriber: ${val}`));
    }, 3000);



    // ReplaySubject emits to any observer all of the items that were
    // emitted by the source Observable(s), regardless of when the
    // observer subscribes and doesn't need to wait for completition
    // to recieve values.
    const replaySubject = new ReplaySubject();
    const replaySeries$ = subject.asObservable();
    replaySeries$.subscribe(val => console.log(`First subscriber: ${val}`));

    replaySubject.next(1);
    replaySubject.next(2);
    replaySubject.next(3);
    // replaySubject.complete();

    setTimeout(() => {
        replaySeries$.subscribe(val => console.log(`Second subscriber: ${val}`));
        replaySubject.next(4);
        replaySubject.complete();
    }, 3000);

  }

}
